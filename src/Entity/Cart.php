<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\CartRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CartRepository::class)
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="json")
     */
    private array $products = [];

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     */
    private int $productCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull
     */
    private int $total = 0;

    /**
     * @ORM\Column(type="string", length=3)
     * @Assert\Currency
     * @Assert\NotNull
     */
    private string $currency = 'USD';

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $added;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $updated;

    public function __construct()
    {
        $this->setAdded();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function setProducts(array $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getProductCount(): int
    {
        return $this->productCount;
    }

    public function setProductCount(int $productCount): self
    {
        $this->productCount = $productCount;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency($currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAdded(): \DateTimeInterface
    {
        return $this->added;
    }

    public function setAdded(): self
    {
        $this->added = new \DateTime("now");

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(): self
    {
        $this->updated = new \DateTime("now");

        return $this;
    }

    public function toArray(): array
    {
        return [
            'cartId'    => $this->getId(),
            'products'  => $this->getProducts(),
            'total'     => $this->getTotal() / 100,
            'currency'  => $this->getCurrency()
        ];
    }
}
