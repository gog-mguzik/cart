<?php
declare(strict_types=1);

namespace App\Message\Query;

use Symfony\Component\Uid\Uuid;

final class Cart
{
    public function __construct(private Uuid $cartId) { }

    public function getCartId(): Uuid
    {
        return $this->cartId;
    }
}
