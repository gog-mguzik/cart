<?php
declare(strict_types=1);

namespace App\Message\Command;

use Symfony\Component\Uid\Uuid;

final class DeleteCart
{
    public function __construct(private Uuid $id) { }

    public function getId(): Uuid
    {
        return $this->id;
    }
}
