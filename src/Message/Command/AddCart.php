<?php
declare(strict_types=1);

namespace App\Message\Command;

use Symfony\Component\Uid\Uuid;

final class AddCart
{
    public function __construct(
        private ?Uuid $id,
        private array $products,
        private int $productsCount,
        private int $total,
        private string $currency
    ) { }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function getProductsCount(): int
    {
        return $this->productsCount;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
