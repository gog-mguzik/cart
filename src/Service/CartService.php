<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Cart;
use App\Exception\CanNotAddMoreProductsToCartException;
use App\Exception\CanNotAddMoreQtyPerProductToCartException;
use App\Exception\CanNotRecalculateCartException;
use App\Exception\EmptyCartException;
use App\Message\Command\DeleteCart;
use App\Message\Query\Cart as CartQuery;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Uid\Uuid;

class CartService
{
    public const MAX_PRODUCTS_IN_CART = 3;
    public const MAX_QTY_PER_PRODUCT  = 10;

    public function __construct(private MessageBusInterface $messageBus) {}

    public function getCart($cartId): Cart
    {
        if ($cartId) {
            $handledStamp = $this->messageBus
                ->dispatch(new CartQuery(Uuid::fromString($cartId)))
                ->last(HandledStamp::class);

            return $handledStamp->getResult();
        }

        return new Cart();
    }

    public function addToCart(Cart $cart, array $product, mixed $qTy): Cart
    {
        try {
            $cart->setProducts($this->addProductToCart($cart, $product, $qTy));

            return $cart;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
    }

    public function removeFromCart(Cart $cart, $productToRemove): Cart|Exception
    {
        try {
            $cartProducts = $cart->getProducts();

            foreach ($cartProducts as $id => $product) {
                if ($product['productId'] === $productToRemove) {
                    unset($cartProducts[$id]);
                }
            }

            $cart->setProducts($cartProducts);

            if (empty($cart->getProductCount())) {
                $this->messageBus
                    ->dispatch(new DeleteCart($cart->getId()))
                    ->last(HandledStamp::class);

                throw new EmptyCartException();
            }

            return $cart;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
    }

    public function canAddMoreProductsToCart(Cart $cart): Cart|CanNotAddMoreProductsToCartException
    {
        return ($cart->getProductCount() > self::MAX_PRODUCTS_IN_CART)
            ? throw new CanNotAddMoreProductsToCartException(self::MAX_PRODUCTS_IN_CART)
            : $cart;
    }

    public function recalculateTotal(Cart $cart): int
    {
        $total = 0;

        foreach ($cart->getProducts() as $product) {
            $total += $product['subtotal'];
        }

        return $total;
    }

    public function countProductsInCart(Cart $cart): int
    {
        return count($cart->getProducts());
    }

    public function recalculateCart(Cart $cart): Cart|Exception
    {
        try {
            $cart->setTotal($this->recalculateTotal($cart));
            $cart->setProductCount($this->countProductsInCart($cart));
            $cart->setUpdated();

            return $cart;
        } catch (Exception $exception) {
            throw new CanNotRecalculateCartException($exception->getMessage(), $exception->getCode());
        }
    }

    private function addProductToCart(Cart $cart, array $product, mixed $qTy): array
    {
        return empty($cart->getProductCount())
            ? $this->addFirstProduct($product, $qTy)
            : $this->addNextProduct($cart, $product, $qTy);
    }

    private function addFirstProduct(array $product, int $qTy): array
    {
        return [$this->getNewProduct($product['id'], $qTy, $product['price'], $product['currency'])];
    }

    private function addNextProduct(Cart $cart, array $product, int $qTy): array
    {
        $cartProducts = $cart->getProducts();

        if ($exist = $this->existProductInCart($cartProducts, $product)) {
            $cartProducts[$exist['id']] = $this->addExistedProductInCart($exist, $product, $qTy);
        } else {
            $cartProducts[] = $this->addNonExistedProductInCart($product, $qTy);
        }

        return $cartProducts;
    }

    private function addExistedProductInCart(array $cartProduct, array $product, int $qTy): array
    {
        return $this->getNewProduct($product['id'], ($cartProduct['qTy'] + $qTy), $product['price'], $product['currency']);
    }

    private function addNonExistedProductInCart(array $product, int $qTy): array
    {
        return $this->getNewProduct($product['id'], $qTy, $product['price'], $product['currency']);
    }

    private function getNewProduct(string $id, int $qTy, int|float $subtotal, string $currency): array
    {
        return [
            'productId' => $id,
            'qTy'       => $this->canAddNextQtyForProduct($qTy),
            'subtotal'  => $this->calculateSubtotal($qTy, $subtotal),
            'currency'  => $currency
        ];
    }

    private function canAddNextQtyForProduct(int $qTy): int|CanNotAddMoreQtyPerProductToCartException
    {
        return ($qTy > self::MAX_QTY_PER_PRODUCT)
            ? throw new CanNotAddMoreQtyPerProductToCartException(self::MAX_QTY_PER_PRODUCT)
            : $qTy;
    }

    private function calculateSubtotal(int $qTy, int|float $price): int
    {
        return (int) round($qTy * ($price * 100));
    }

    private function existProductInCart(array $cartProducts, array $newProduct): array
    {
        $exist = [];

        foreach ($cartProducts as $id => $cartProduct) {
            if ($cartProduct['productId'] === $newProduct['id']) {
                $exist['id']  = $id;
                $exist['qTy'] = $cartProduct['qTy'];
            }
        }

        return $exist;
    }
}
