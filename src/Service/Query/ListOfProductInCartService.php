<?php
declare(strict_types=1);

namespace App\Service\Query;

use App\Helper\RequestHelper;
use App\Service\CartService;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class ListOfProductInCartService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Executed successfully.';

    public function __construct(
        private RequestHelper $requestHelper,
        private CartService $cartService,
        private MessageBusInterface $messageBus,
    ) {}

    public function process(Request $request): JsonResponse
    {
        try {
            $cartId = $this->requestHelper->getHeaders($request)->get('Cart-Id');
            $cart = $this->cartService->getCart($cartId);

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_OK,
                $cart->toArray()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
