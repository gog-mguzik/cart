<?php
declare(strict_types=1);

namespace App\Service;

use App\Exception\CartNotGetProductException;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductService
{
    private const API_VERSION = 'v1';

    public function __construct(private ContainerBagInterface $containerBag) { }

    public function getProduct(string $productId, ?string $currency): array|CartNotGetProductException
    {
        try {
            $client = new Client();

            $response = $client->request(
                Request::METHOD_GET,
                sprintf(
                    '%s/%s/products/%s?currency=%s',
                    $this->containerBag->get('app.catalogue_path'), self::API_VERSION, $productId, $currency
                )
            );

            if ($response->getStatusCode() === Response::HTTP_OK) {
                return json_decode((string) $response->getBody(), true)['body'];
            }

            throw new CartNotGetProductException($productId, $response->getReasonPhrase());
        } catch (\Exception $exception) {
            throw new CartNotGetProductException($productId, $exception->getMessage());
        }
    }
}
