<?php
declare(strict_types=1);

namespace App\Service;

class ServiceResponse implements ServiceResponseInterface
{
    public function parseResponse($message, $code, $content = []): array
    {
        return [
            'code'      => $code,
            'message'   => $message,
            'body'      => $content
        ];
    }
}
