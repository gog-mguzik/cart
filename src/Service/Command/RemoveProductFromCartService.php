<?php
declare(strict_types=1);

namespace App\Service\Command;

use App\Helper\RequestHelper;
use App\Message\Command\AddCart as UpdateCartCommand;
use App\Service\CartService;
use App\Service\ProductService;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Uid\Uuid;

class RemoveProductFromCartService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Product deleted successfully.';

    public function __construct(
        private RequestHelper $requestHelper,
        private CartService $cartService,
        private ProductService $productService,
        private MessageBusInterface $messageBus
    ) {}

    public function process(Request $request): JsonResponse
    {
        try {
            $cartId = $this->requestHelper->getHeaders($request)->get('Cart-Id');
            $cart = $this->cartService->getCart($cartId);

            $requestParams = $this->requestHelper->getRequestParams($request);

            $cart = $this->cartService->removeFromCart($cart, $requestParams['productId']);
            $cart = $this->cartService->recalculateCart($cart);

            $cartCommand = $this->messageBus
                ->dispatch(new UpdateCartCommand(
                    $cartId ? Uuid::fromString($cartId) : null,
                    $cart->getProducts(),
                    $cart->getProductCount(),
                    $cart->getTotal(),
                    $cart->getCurrency()
                ))
                ->last(HandledStamp::class)
                ->getResult();

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_OK,
                $cartCommand->toArray()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
