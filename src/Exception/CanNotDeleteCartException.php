<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CanNotDeleteCartException extends \Exception
{
    public function __construct(protected $infoMessage)
    {
        parent::__construct(
            sprintf('Can\'t add cart because: %s', $this->infoMessage),
            Response::HTTP_BAD_REQUEST
        );
    }
}