<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CanNotAddMoreQtyPerProductToCartException extends \Exception
{
    public function __construct(protected int $maxQty)
    {
        parent::__construct(
            sprintf('Can\'t add more qTy than %s for product to cart.', $this->maxQty),
            Response::HTTP_NOT_ACCEPTABLE
        );
    }
}
