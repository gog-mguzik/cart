<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CartNotFoundException extends \Exception
{
    public function __construct(protected string $cartId)
    {
        parent::__construct(
            sprintf('NOT found cart ID: %s.', $this->cartId),
            Response::HTTP_NOT_FOUND
        );
    }
}
