<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CartNotGetProductException extends \Exception
{
    public function __construct(protected string $productId, protected string $infoMessage)
    {
        parent::__construct(
            sprintf('Can\'t get product ID: %s because: %s', $this->productId, $infoMessage),
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
