<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class EmptyCartException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            'Empty cart. Add product to cart to create new one.',
            Response::HTTP_OK
        );
    }
}