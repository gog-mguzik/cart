<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CanNotAddMoreProductsToCartException extends \Exception
{
    public function __construct(protected int $maxProductInCart)
    {
        parent::__construct(
            sprintf('Can\'t add more than %s products to cart.', $this->maxProductInCart),
            Response::HTTP_NOT_ACCEPTABLE
        );
    }
}
