<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\RemoveProductFromCartService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('cart', name: 'cart_v1_remove_product', methods: [Request::METHOD_DELETE])]
class RemoveProductFromCartController
{
    public function __construct(private RemoveProductFromCartService $service) { }

    public function __invoke(Request $request): JsonResponse
    {
        return $this->service->process($request);
    }
}
