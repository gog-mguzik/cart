<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\AddProductToCartService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('cart', name: 'cart_v1_add_new_product', methods: [Request::METHOD_POST])]
class AddProductToCartController
{
    public function __construct(private AddProductToCartService $service) { }

    public function __invoke(Request $request): JsonResponse
    {
        return $this->service->process($request);
    }
}
