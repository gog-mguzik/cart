<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Query\ListOfProductInCartService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('cart', name: 'cart_v1_list_of_product', methods: [Request::METHOD_GET])]
class ListOfProductInCartController
{
    public function __construct(private ListOfProductInCartService $service) { }

    public function __invoke(Request $request): JsonResponse
    {
        return $this->service->process($request);
    }
}
