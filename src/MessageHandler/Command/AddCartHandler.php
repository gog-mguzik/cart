<?php
declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\Entity\Cart;
use App\Exception\CanNotAddCartException;
use App\Message\Command\AddCart as AddCartCommand;
use App\Repository\CartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddCartHandler implements MessageHandlerInterface
{
    public function __construct(private CartRepository $cartRepository) { }

    public function __invoke(AddCartCommand $addCartCommand): Cart|CanNotAddCartException
    {
        try {
            if (!is_null($addCartCommand->getId())) {
                $cart = $this->cartRepository->find($addCartCommand->getId());
            } else {
                $cart = new Cart();
            }

            $cart->setProducts($addCartCommand->getProducts());
            $cart->setProductCount($addCartCommand->getProductsCount());
            $cart->setTotal($addCartCommand->getTotal());
            $cart->setCurrency($addCartCommand->getCurrency());
            $cart->setUpdated();

            $this->cartRepository->save($cart);

            return $cart;
        } catch (\Exception $exception) {
            throw new CanNotAddCartException($exception);
        }
    }
}
