<?php
declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\Exception\CanNotDeleteCartException;
use App\Message\Command\DeleteCart as DeleteCartCommand;
use App\Repository\CartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteCartHandler implements MessageHandlerInterface
{
    public function __construct(private CartRepository $cartRepository) { }

    public function __invoke(DeleteCartCommand $deleteCart): bool|CanNotDeleteCartException
    {
        try {
            $this->cartRepository->delete(
                $this->cartRepository->find($deleteCart->getId())
            );

            return true;
        } catch (\Exception $exception) {
            throw new CanNotDeleteCartException($exception);
        }
    }
}
