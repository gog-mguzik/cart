<?php
declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\Entity\Cart;
use App\Exception\CartNotFoundException;
use App\Message\Query\Cart as CartQuery;
use App\Repository\CartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CartHandler implements MessageHandlerInterface
{
    public function __construct(private CartRepository $cartRepository) { }

    public function __invoke(CartQuery $cartQuery): Cart|CartNotFoundException
    {
        $cart = $this->cartRepository->find($cartQuery->getCartId());

        if (is_null($cart)) {
            throw new CartNotFoundException((string) $cartQuery->getCartId());
        }

        return $cart;
    }
}
