<?php

namespace App\Repository;

use App\Entity\Cart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cart|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cart|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cart[]    findAll()
 * @method Cart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cart::class);
    }

    public function save(Cart $cart)
    {
        try {
            $this->_em->persist($cart);
            $this->_em->flush();

            return $cart;
        } catch (ORMException $exception) {

        }
    }

    public function delete(Cart $cart): void
    {
        try {
            $this->_em->remove($cart);
            $this->_em->flush();
        } catch (ORMException $exception) {

        }
    }
}
