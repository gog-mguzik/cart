# API Cart
API wystawiłem do testów. Można na nie uderzać Postmanem (konfiguracja do  [pobrania](https://www.getpostman.com/collections/7d253bfca7c8256486a8))

### Routes
- Add product to cart
- List all the products in the cart
- Remove product from the cart

### Add product to cart
Pierwsze dodanie produktu do koszyka dodatkowo tworzy go. Po utworzeniu koszyka i dodaniu produktu do niego, zwracany jest `cartId`, który należy dodać do `header`u. Każde kolejne dodanie produktu dodaje go do już istniejącego koszyka.
Przykładowy Request:
```
{
    "productId": "9a15aaca-84c9-4fbb-82cd-2debcb1da42c",
    "qTy": 1
}
```
`productId` pobierany jest z API Catalogue. Musi to być istniejące productId w ms:cataloguje - przy każdym dodaniu do koszyka, walidowane jest, czy dany produkt istnieje.
### List all the products in the cart
Wyświetlenie listy produktów następuje po uderzeniu `GET`em - musi być dodany `header`: `Cart-Id`: UUID koszyka.

###  Remove product from the cart
Usunięcie produktu z koszyka usuwa przekazany `productId` z koszyka. Jeśli to jest ostatni produkt w koszyku - koszyk też zostanie usunięty.